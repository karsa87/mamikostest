<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\KostController;
use App\Http\Controllers\Api\KostDiscussionController;
use App\Http\Controllers\Api\MediaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('api.')->group(function () {
    Route::post('/register', [AuthController::class, 'register'])->name('auth.register');
    Route::post('/login', [AuthController::class, 'login'])->name('auth.login');
    Route::prefix('kost')->name('kost.')->group(function () {
        Route::get('search', [KostController::class, 'search'])->name('search');
        Route::get('show/{slug}', [KostController::class, 'show'])->name('show');
    });

    Route::middleware('auth:sanctum')->group(function () {
        Route::prefix('kost')->name('kost.')->group(function () {
            Route::get('list', [KostController::class, 'index'])->name('list');
            Route::post('add', [KostController::class, 'store'])->name('add');
            Route::put('update/{kost}', [KostController::class, 'update'])->name('update');
            Route::delete('delete/{kost}', [KostController::class, 'destroy'])->name('delete');
            Route::post('upload-media', [MediaController::class, 'upload'])->name('upload.media');
        });

        Route::prefix('kost-discussion')->name('kost.discussion.')->middleware('can.discuss')->group(function () {
            Route::post('add', [KostDiscussionController::class, 'add'])->name('add');
            Route::post('reply/{kost_discussion}', [KostDiscussionController::class, 'reply'])->name('reply');
        });
    });
});
