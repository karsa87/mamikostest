<?php

return [
    'point' => [
        'user' => [
            'regular' => 20,
            'premium' => 40,
        ],
        'discussion' => [
            'credit' => 5,
        ],
    ],
];
