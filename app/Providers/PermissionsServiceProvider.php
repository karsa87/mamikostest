<?php

namespace App\Providers;

use App\Policies\RolePolicies;
use Illuminate\Support\ServiceProvider;

class PermissionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            RolePolicies::define();
        } catch (\Exception $e) {
            return false;
        }
    }
}
