<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KostDiscussion extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'reply_id',
        'kost_id',
        'text',
    ];

    /**
     * Relation.
     * **/
    public function kost()
    {
        return $this->belongsTo(Kost::class);
    }

    public function reply()
    {
        return $this->belongsTo(self::class, 'reply_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /***
     * Event
     * **/
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($discussion) {
            $discussion->user->minusPoint(config('default.point.discussion.credit'), 'Discussion');
        });
    }
}
