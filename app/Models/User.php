<?php

namespace App\Models;

use App\Trait\HasPermissionTrait;
use App\Trait\PointLoggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasPermissionTrait, PointLoggable;

    const STATUS_ACTIVE = true;
    const STATUS_DEACTIVE = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'status',
        'point',
        'phone',
        'address',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Accessor.
     * **/
    public function getIsOwnerAttribute()
    {
        return $this->role && $this->role->type == Role::TYPE_OWNER;
    }

    public function getIsRegularAttribute()
    {
        return $this->role && $this->role->type == Role::TYPE_REGULAR_USER;
    }

    public function getIsPremiumAttribute()
    {
        return $this->role && $this->role->type == Role::TYPE_PREMIUM_USER;
    }

    /**
     * Scope.
     * **/
    public function scopeActive($query)
    {
        return $query->whereStatus(self::STATUS_ACTIVE);
    }
}
