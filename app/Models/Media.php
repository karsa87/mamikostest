<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Media extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
        'description',
        'path',
    ];

    protected $table = 'medias';

    /**
     * Relation.
     * **/
    public function kosts()
    {
        return $this->belongsToMany(Kost::class, 'kost_medias');
    }

    /**
     * Accessor.
     * **/
    public function getFullUrlAttribute()
    {
        return Storage::url($this->path);
    }
}
