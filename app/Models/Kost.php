<?php

namespace App\Models;

use App\Trait\ScopeLike;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kost extends Model
{
    use HasFactory, ScopeLike;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'slug',
        'owner_id',
        'phone',
        'address',
        'city',
        'province',
        'country',
        'price',
        'description',
    ];

    /**
     * Relation.
     * **/
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function medias()
    {
        return $this->belongsToMany(Media::class, 'kost_medias');
    }

    public function kostDiscussions()
    {
        return $this->hasMany(KostDiscussion::class);
    }
}
