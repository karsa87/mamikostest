<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;

class ResetUserPoint extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:user-point';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recharge user point';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->confirm('Are you sure for reset point all user ?', true)) {
            // reset owner
            User::whereHas('role', function ($q) {
                $q->where('type', '=', Role::TYPE_OWNER);
            })->update([
                'point' => 0,
            ]);

            // reset not owner
            $users = User::whereHas('role', function ($q) {
                $q->where('type', '!=', Role::TYPE_OWNER);
            })->get();
            $bar = $this->output->createProgressBar($users->count());
            $bar->start();
            foreach ($users as $user) {
                $user->resetPoint();
                $bar->advance();
            }
            $bar->finish();

            return Command::SUCCESS;
        }

        return Command::FAILURE;
    }
}
