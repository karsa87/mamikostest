<?php

namespace App\Trait;

use App\Models\PointLog;

trait PointLoggable
{
    public function addPoint($point = 0, $description = '')
    {
        if ($this->is_owner) {
            return;
        }

        PointLog::create([
            'user_id' => $this->id,
            'point' => $point,
            'description' => sprintf('Add - %s', $description),
        ]);

        $this->point += $point;
        $this->save();
    }

    public function minusPoint($point = 5, $description = '')
    {
        if ($this->is_owner) {
            return;
        }

        $point = abs($point);

        PointLog::create([
            'user_id' => $this->id,
            'point' => -1 * $point,
            'description' => sprintf('Minus - %s', $description),
        ]);

        $this->point -= $point;
        $this->save();
    }

    public function resetPoint()
    {
        if ($this->is_owner) {
            return;
        }

        if ($this->point > 0) {
            $this->minusPoint($this->point, 'Reset point');
        }

        if ($this->is_regular) {
            $this->addPoint(config('default.point.user.regular'), 'Reset point');
        } elseif ($this->is_premium) {
            $this->addPoint(config('default.point.user.premium'), 'Reset point');
        }
    }
}
