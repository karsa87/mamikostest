<?php

namespace App\Trait;

use App\Models\Permission;
use App\Models\Role;

trait HasPermissionTrait
{
    public function hasPermissionTo($permission)
    {
        return $this->hasPermissionThroughRole($permission);
    }

    public function hasPermissionThroughRole($permission)
    {
        foreach ($permission->roles as $role) {
            if ($this->role && $this->role->slug == $role->slug) {
                return true;
            }
        }

        return false;
    }

    public function hasRole(...$roles)
    {
        foreach ($roles as $role) {
            if ($this->role && $this->role->slug == $role->slug) {
                return true;
            }
        }

        return false;
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    protected function getAllPermissions(array $permissions)
    {
        return Permission::whereIn('slug', $permissions)->get();
    }
}
