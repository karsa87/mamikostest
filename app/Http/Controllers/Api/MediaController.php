<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Media;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    /**
     * Upload Media.
     *
     * @param  \App\Models\Kost  $kost
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        if (!$request->hasFile('media')) {
            return response()->json([
                'status' => false,
                'messages' => [
                    'File not found',
                ],
                'data' => [],
            ], 400);
        }

        $allowedfileExtension = ['pdf', 'jpg', 'jpeg', 'png'];
        $file = $request->file('media');
        $extension = $file->getClientOriginalExtension();

        $media = new Media();
        if (in_array($extension, $allowedfileExtension)) {
            $uploadedFile = $file;
            $title = $request->get('title', null);
            $filename = $request->get('title', null) ? sprintf('%s.%s', $title, $extension) : $file->getClientOriginalName();
            $filename = preg_replace('/^([^\\\\]*)\.(\w+)$/', '$1-' . strtotime(date('Y-m-d H:i:s')) . '.$2', $filename);
            $filename = strtolower(preg_replace('/[^a-zA-Z0-9.\/]/', '-', $filename));
            $destination = '/medias';

            $path = \Storage::putFileAs(
                $destination,
                $uploadedFile,
                $filename
            );

            //store image file into directory and db
            $media->title = $title ?? $filename;
            $media->description = $request->get('description', '');
            $media->path = $path;
            $media->save();
        } else {
            return response()->json([
                'status' => false,
                'messages' => [
                    'Invalid format file',
                ],
                'data' => [],
            ], 422);
        }

        return response()->json([
            'success' => true,
            'messages' => [],
            'data' => [
                'id' => $media->id,
                'title' => $media->title,
                'description' => $media->description,
                'path' => $media->path,
                'full_url' => $media->full_url,
            ],
        ]);
    }
}
