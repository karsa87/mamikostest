<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $api_key = $request->header('x-api-key');

        if ($api_key == config('sanctum.secret_key')) {
            $input = $request->only([
                'name',
                'email',
                'phone',
                'address',
                'password',
            ]);
            $role = Role::whereType($request->role)->first();

            $user = new User();
            $user->fill($input);
            $user->password = Hash::make($input['password']);
            $user->status = User::STATUS_ACTIVE;
            $user->role_id = $role ? $role->id : null;
            $user->save();

            // Given point for new user
            if ($user->is_regular) {
                $user->addPoint(config('default.point.user.regular'), 'Register');
            } elseif ($user->is_premium) {
                $user->addPoint(config('default.point.user.premium'), 'Register');
            }

            $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json([
                'status' => true,
                'messages' => [],
                'data' => [
                    'access_token' => $token,
                    'token_type' => 'Bearer',
                ],
            ]);
        } else {
            throw new HttpResponseException(response()->json([
                'status' => false,
                'messages' => [
                    'Unauthorized',
                ],
                'data' => [],
            ], 401));
        }
    }

    public function login(LoginRequest $request)
    {
        $api_key = $request->header('x-api-key');

        if ($api_key == config('sanctum.secret_key')) {
            if (!Auth::attempt($request->only('email', 'password'))) {
                return response()->json([
                    'status' => false,
                    'messages' => [
                        'Invalid login details',
                    ],
                    'data' => [],
                ], 401);
            }

            $user = User::where('email', $request->email)->active()->first();
            if (empty($user)) {
                throw new HttpResponseException(response()->json([
                    'status' => false,
                    'messages' => ['User account deactivated'],
                    'data' => [],
                ], 401));
            }

            $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json([
                'status' => true,
                'messages' => [],
                'data' => [
                    'access_token' => $token,
                    'token_type' => 'Bearer',
                ],
            ]);
        } else {
            throw new HttpResponseException(response()->json([
                'status' => false,
                'messages' => [
                    'Unauthorized',
                ],
                'data' => [],
            ], 401));
        }
    }
}
