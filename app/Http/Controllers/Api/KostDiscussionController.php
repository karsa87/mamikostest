<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\KostDiscussionRequest;
use App\Models\KostDiscussion;

class KostDiscussionController extends Controller
{
    /**
     * Add a discussion.
     *
     * @param  \App\Http\Requests\Api\KostDiscussionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function add(KostDiscussionRequest $request)
    {
        $input = $request->validated();

        $discussion = new KostDiscussion();
        $discussion->fill($input);
        $discussion->save();

        return response()->json([
            'success' => true,
            'messages' => [],
            'data' => [
                'id' => $discussion->id,
                'text' => $discussion->text,
                'user' => [
                    'name' => $discussion->user->name,
                    'email' => $discussion->user->email,
                    'phone' => $discussion->user->phone,
                    'address' => $discussion->user->address,
                ],
                'kost' => [
                    'name' => $discussion->kost->name,
                    'phone' => $discussion->kost->phone,
                    'address' => $discussion->kost->address,
                ],
            ],
        ]);
    }

    /**
     * Reply a discussion.
     *
     * @param  \App\Http\Requests\Api\KostDiscussionRequest  $request
     * @param  \App\Models\KostDiscussion  $kostDiscussion
     * @return \Illuminate\Http\Response
     */
    public function reply(KostDiscussionRequest $request, KostDiscussion $kostDiscussion)
    {
        $input = $request->validated();
        $input['reply_id'] = $kostDiscussion->id;

        $discussion = new KostDiscussion();
        $discussion->fill($input);
        $discussion->save();

        return response()->json([
            'success' => true,
            'messages' => [],
            'data' => [
                'id' => $discussion->id,
                'text' => $discussion->text,
                'user' => [
                    'name' => $discussion->user->name,
                    'email' => $discussion->user->email,
                    'phone' => $discussion->user->phone,
                    'address' => $discussion->user->address,
                ],
                'kost' => [
                    'name' => $discussion->kost->name,
                    'phone' => $discussion->kost->phone,
                    'address' => $discussion->kost->address,
                ],
                'reply' => [
                    'id' => $discussion->reply->id,
                    'text' => $discussion->reply->text,
                    'user' => [
                        'name' => $discussion->reply->user->name,
                        'email' => $discussion->reply->user->email,
                        'phone' => $discussion->reply->user->phone,
                        'address' => $discussion->reply->user->address,
                    ],
                    'kost' => [
                        'name' => $discussion->reply->kost->name,
                        'phone' => $discussion->reply->kost->phone,
                        'address' => $discussion->reply->kost->address,
                    ],
                ],
            ],
        ]);
    }
}
