<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\KostRequest;
use App\Models\Kost;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class KostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = $this->get_query($request)->where('owner_id', auth()->user()->id);
        $kosts = $query->simplePaginate(15)->appends($request->except(['page']));

        return response()->json([
            [
                'success' => true,
                'message' => [],
                'data' => [
                    'list' => $kosts->items(),
                    'current_page' => $kosts->currentPage(),
                    'per_page' => $kosts->perPage(),
                    'prev_page_url' => $kosts->previousPageUrl(),
                    'next_page_url' => $kosts->nextPageUrl(),
                ],
            ],
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $query = $this->get_query($request)
                    ->select('name', 'slug', 'owner_id', 'phone', 'address', 'city', 'province', 'country', 'price', 'description');
        $query->with('owner', function ($q) {
            $q->select('id', 'name', 'email', 'phone', 'address');
        });

        $kosts = $query->simplePaginate(15)->appends($request->except(['page']));

        $lists = collect();
        foreach ($kosts->items() as $kost) {
            $lists->push([
                'name' => $kost->name,
                'slug' => $kost->slug,
                'phone' => $kost->phone,
                'address' => $kost->address,
                'city' => $kost->city,
                'province' => $kost->province,
                'country' => $kost->country,
                'price' => $kost->price,
                'description' => $kost->description,
                'owner' => [
                    'name' => $kost->owner->name,
                    'email' => $kost->owner->email,
                    'phone' => $kost->owner->phone,
                    'address' => $kost->owner->address,
                ],
            ]);
        }

        return response()->json([
            [
                'success' => true,
                'message' => [],
                'data' => [
                    'list' => $lists,
                    'current_page' => $kosts->currentPage(),
                    'per_page' => $kosts->perPage(),
                    'prev_page_url' => $kosts->previousPageUrl(),
                    'next_page_url' => $kosts->nextPageUrl(),
                ],
            ],
        ]);
    }

    protected function get_query(Request $request)
    {
        $query = Kost::query();

        if ($name = $request->get('name', '')) {
            $query->whereLike('name', $name);
        }

        if ($location = $request->get('location', '')) {
            $query->where(function ($q) use ($location) {
                $q->whereLike('address', $location)
                    ->orWhereLike('city', $location)
                    ->orWhereLike('province', $location)
                    ->orWhereLike('country', $location);
            });
        }

        if ($city = $request->get('city', '')) {
            $query->whereLike('city', $city);
        }

        if ($province = $request->get('province', '')) {
            $query->whereLike('province', $province);
        }

        if ($country = $request->get('country', '')) {
            $query->whereLike('country', $country);
        }

        if (is_numeric($request->get('min-price', null))) {
            $query->where('price', '>=', $request->get('min-price', 0));
        }

        if (is_numeric($request->get('max-price', null))) {
            $query->where('price', '<=', $request->get('max-price', 0));
        }

        $sort = $request->get('sort', 'asc');
        $sort = is_string($sort) ? strtolower($sort) : $sort;
        $sort = in_array($sort, ['asc', 'desc']) ? $sort : 'asc';
        $sortBy = $request->get('sortBy', 'price');
        $query->orderBy($sortBy, $sort);

        return $query;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Api\KostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KostRequest $request)
    {
        $input = $request->validated();
        $kost = new Kost();
        $kost->fill($input);
        $kost->owner_id = auth()->user()->id;

        if ($kost->save() && $request->medias) {
            $kost->medias()->sync($request->medias);
        }

        $data = $kost->toArray();
        $data['medias'] = collect();
        foreach ($kost->medias as $media) {
            $data['medias']->push([
                'id' => $media->id,
                'title' => $media->title,
                'description' => $media->description,
                'path' => $media->path,
                'full_url' => $media->full_url,
            ]);
        }

        return response()->json([
            'success' => true,
            'messages' => [],
            'data' => $data,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $kost = Kost::with([
            'owner',
            'medias',
            'kostDiscussions',
        ])->whereSlug($slug)->firstOrFail();

        $data = [
            'name' => $kost->name,
            'slug' => $kost->slug,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => round($kost->price, 2),
            'description' => $kost->description,
            'owner' => [
                'name' => $kost->owner->name,
                'email' => $kost->owner->email,
                'phone' => $kost->owner->phone,
                'address' => $kost->owner->address,
            ],
            'medias' => [],
            'kost_discussions' => [],
        ];

        foreach ($kost->medias as $media) {
            $data['medias'][] = [
                'title' => $media->title,
                'description' => $media->description,
                'path' => $media->path,
                'full_url' => $media->full_url,
            ];
        }

        foreach ($kost->kostDiscussions as $discuss) {
            $v = [
                'user' => [
                    'name' => $discuss->user->name,
                    'email' => $discuss->user->email,
                    'phone' => $discuss->user->phone,
                ],
                'text' => $discuss->text,
            ];

            if ($discuss->reply) {
                $v['reply'] = [
                    'user' => [
                        'name' => $discuss->reply->user->name,
                        'email' => $discuss->reply->user->email,
                        'phone' => $discuss->reply->user->phone,
                    ],
                    'text' => $discuss->reply->text,
                ];
            }

            $data['kost_discussions'][] = $v;
        }

        return response()->json([
            'status' => true,
            'messages' => [],
            'data' => $data,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Api\KostRequest  $request
     * @param  \App\Models\Kost  $kost
     * @return \Illuminate\Http\Response
     */
    public function update(KostRequest $request, Kost $kost)
    {
        if ($kost->owner_id != auth()->user()->id) {
            throw new HttpResponseException(response()->json([
                'status' => false,
                'messages' => [
                    'You dont have access update this kost detail',
                ],
                'data' => [],
            ], 401));
        }

        $input = $request->validated();
        $kost->fill($input);
        $kost->save();

        if ($kost->save() && $request->medias) {
            $kost->medias()->syncWithoutDetaching($request->medias);
        }

        $data = $kost->toArray();
        $data['medias'] = collect();
        foreach ($kost->medias as $media) {
            $data['medias']->push([
                'id' => $media->id,
                'title' => $media->title,
                'description' => $media->description,
                'path' => $media->path,
                'full_url' => $media->full_url,
            ]);
        }

        return response()->json([
            'success' => true,
            'messages' => [],
            'data' => $data,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kost  $kost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kost $kost)
    {
        if ($kost->owner_id != auth()->user()->id) {
            throw new HttpResponseException(response()->json([
                'status' => false,
                'messages' => [
                    'You dont have access delete this kost',
                ],
                'data' => [],
            ], 401));
        }

        $kost->delete();

        return response()->json([
            'success' => true,
            'messages' => [
                'Success delete the kost',
            ],
            'data' => [],
        ]);
    }
}
