<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class CanDiscuss
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();

        if (!$user->is_owner && ($user->point - config('default.point.discussion.credit')) < 0) {
            throw new HttpResponseException(response()->json([
                'status' => false,
                'messages' => [
                    'You dont have enough point for ask',
                ],
                'data' => [],
            ], 422));
        }

        return $next($request);
    }
}
