<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class KostDiscussionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('add-discussion', 'reply-discussion');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => [
                'required',
                'numeric',
                'exists:users,id',
            ],
            'kost_id' => [
                'required',
                'numeric',
                'exists:kosts,id',
            ],
            'text' => [
                'required',
                'string',
            ],
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'user_id' => auth()->user()->id,
            'text' => htmlentities($this->text ?? ''),
        ]);
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $message = [];
        foreach ($validator->errors()->toArray() as $errors) {
            foreach ($errors as $err) {
                $message[] = $err;
            }
        }

        throw new HttpResponseException(response()->json([
            'status' => false,
            'messages' => $message,
            'data' => [],
        ], 422));
    }
}
