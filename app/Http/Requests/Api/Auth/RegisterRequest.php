<?php

namespace App\Http\Requests\Api\Auth;

use App\Models\Role;
use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'max:255',
            ],
            'password' => [
                'required',
                'min:7',
                'regex:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,255}$/',
            ],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                'unique:users',
            ],
            'phone' => [
                'nullable',
                'regex:/^\+?[0-9]+$/',
                'max:16',
            ],
            'role' => [
                'required',
                Rule::in([
                    Role::TYPE_OWNER,
                    Role::TYPE_REGULAR_USER,
                    Role::TYPE_PREMIUM_USER,
                ]),
            ],
            'address' => [
                'nullable',
            ],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.regex' => 'Invalid password, minimum 7 characters containing uppercase, lowercase and numbers',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $message = [];
        foreach ($validator->errors()->toArray() as $errors) {
            foreach ($errors as $err) {
                $message[] = $err;
            }
        }

        throw new HttpResponseException(response()->json([
            'status' => false,
            'messages' => $message,
            'data' => [],
        ], 422));
    }
}
