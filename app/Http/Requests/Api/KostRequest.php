<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Str;

class KostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('add-kost', 'update-kost');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->kost->id ?? 'NULL';

        return [
            'name' => [
                'required',
                'string',
            ],
            'slug' => [
                'required',
                'string',
                'unique:kosts,slug,' . $id . ',id',
            ],
            'phone' => [
                'nullable',
                'regex:/^\+?[0-9]+$/',
                'max:16',
            ],
            'address' => [
                'required',
                'string',
            ],
            'city' => [
                'required',
                'string',
            ],
            'province' => [
                'required',
                'string',
            ],
            'country' => [
                'nullable',
                'string',
            ],
            'description' => [
                'nullable',
                'string',
            ],
            'price' => [
                'required',
                'numeric',
                'min:0',
            ],
            'medias' => [
                'nullable',
                'array',
            ],
            'medias.*' => [
                'nullable',
                'numeric',
            ],
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'description' => htmlentities($this->description ?? ''),
            'slug' => Str::slug($this->name),
        ]);
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'medias.*' => 'The medias must array with numeric value.',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $message = [];
        foreach ($validator->errors()->toArray() as $errors) {
            foreach ($errors as $err) {
                $message[] = $err;
            }
        }

        throw new HttpResponseException(response()->json([
            'status' => false,
            'messages' => $message,
            'data' => [],
        ], 422));
    }
}
