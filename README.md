## Laravel Test
Test - Mamikos - Software Engineer
## Requirement
- PHP >= 7.3
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- MySql
- MySql PHP Extension
- Composer

## How to install
- Clone this repository
- Create file `.env` with copy from `.env.example`, then change configure `.env` for compatible with your server.
- Install vendor via composer use this command `composer install`
- Install npm use this command `npm install`
- Generate file js dan css this command `npm run prod`
- Change permission folder `storage` use this command `chmod -R 777 storage`
- Run `php artisan storage:link` for generate link public storage
- Run `php artisan migrate --seed` for migrate database structure and default data.
- Configure domain. if you use valet then run command `valet link mamikos`
- Enable ssl for this application. if you use valet then run command `valet secure mamikos`
- Check your domain with open from browser, if you use valet open this link [https://mamikos.test](https://mamikos.test)

## API Documentation
- Open [Swagger Editor](https://editor.swagger.io/)
- Upload file in directory `documentation/api_documentation.yaml`
- Entry you base_url, then Authorize input API Secret Key `%m4mik0s%`, if success then close popup.
- Register your user, using Try it out.
- After Register, Login using email and password registered. if successfully you will get access token
- Then Authorize again input access token to `bearerAuth`. if success Authorize close the popup.
- Now you can use all API

## Command Documentation
`php artisan test` => phpunit test
`composer cs:fix` => style code
`reset:user-point` => command reset point user

## Information About Developer
Developer :
Mochamad Karsa Syaifudin Jupri - Software Engineer

Repository :
- [Btbucket](https://bitbucket.org/karsa87/).
- [Github](https://github.com/karsa87).

Portofolio :
[Posmaster](http://posmaster.wikaproduction.com)
    - username => mamikos
    - password => mamikos

CV :
[PDF](https://drive.google.com/file/d/1eYTAEJTFq8vXTijz7xhrKIEZE_G5rCOw/view?usp=sharing)
