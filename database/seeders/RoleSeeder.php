<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Owner',
                'slug' => 'owner',
                'type' => Role::TYPE_OWNER,
            ], [
                'name' => 'Regular',
                'slug' => 'regular',
                'type' => Role::TYPE_REGULAR_USER,
            ], [
                'name' => 'Premium',
                'slug' => 'permium',
                'type' => Role::TYPE_PREMIUM_USER,
            ],
        ];

        foreach ($roles as $role) {
            Role::updateOrCreate(
                [
                    'slug' => $role['slug'],
                ],
                $role
            );
        }
    }
}
