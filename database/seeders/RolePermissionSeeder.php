<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'See List Kost',
                'slug' => 'see-list-kost',
            ], [
                'name' => 'Add Kost',
                'slug' => 'add-kost',
            ], [
                'name' => 'Update Kost',
                'slug' => 'update-kost',
            ], [
                'name' => 'Delete Kost',
                'slug' => 'delete-kost',
            ], [
                'name' => 'Add Discussion',
                'slug' => 'add-discussion',
            ], [
                'name' => 'Reply Discussion',
                'slug' => 'reply-discussion',
            ],
        ];

        foreach ($permissions as $permission) {
            Permission::updateOrCreate(
                [
                    'slug' => $permission['slug'],
                ],
                $permission
            );
        }

        $this->syncPermissionRoleOwner();
        $this->syncPermissionRoleRegular();
        $this->syncPermissionRolePermium();
    }

    public function syncPermissionRoleOwner()
    {
        $permissions = Permission::whereIn('slug', [
            'add-kost',
            'update-kost',
            'delete-kost',
            'see-list-kost',
            'add-discussion',
            'reply-discussion',
        ])->get();

        $role = Role::whereType(Role::TYPE_OWNER)->first();
        $role->permissions()->sync($permissions->pluck('id'));
    }

    public function syncPermissionRoleRegular()
    {
        $permissions = Permission::whereIn('slug', [
            'add-discussion',
            'reply-discussion',
        ])->get();

        $role = Role::whereType(Role::TYPE_REGULAR_USER)->first();
        $role->permissions()->sync($permissions->pluck('id'));
    }

    public function syncPermissionRolePermium()
    {
        $permissions = Permission::whereIn('slug', [
            'add-discussion',
            'reply-discussion',
        ])->get();

        $role = Role::whereType(Role::TYPE_REGULAR_USER)->first();
        $role->permissions()->sync($permissions->pluck('id'));
    }
}
