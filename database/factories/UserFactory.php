<?php

namespace Database\Factories;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$04$othXd85O4Gwp8ppqOVO/OeEmos7K9oFpuHyT6aUk0tD115BSmlJPC', // P4ssword
            'remember_token' => Str::random(10),
            'status' => User::STATUS_ACTIVE,
            'role_id' => Role::factory()->create(),
            'point' => $this->faker->numberBetween(0, 100),
            'phone' => $this->faker->e164PhoneNumber(),
            'address' => $this->faker->address(),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }

    /**
     * Indicate that the model's status deactivated.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function deactive()
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => User::STATUS_DEACTIVE,
            ];
        });
    }

    /**
     * Indicate that the model's status deactivated.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function regular()
    {
        return $this->state(function (array $attributes) {
            return [
                'role_id' => Role::factory()->regular()->create(),
            ];
        });
    }

    /**
     * Indicate that the model's status deactivated.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function premium()
    {
        return $this->state(function (array $attributes) {
            return [
                'role_id' => Role::factory()->premium()->create(),
            ];
        });
    }
}
