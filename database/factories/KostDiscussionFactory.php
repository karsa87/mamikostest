<?php

namespace Database\Factories;

use App\Models\Kost;
use App\Models\KostDiscussion;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class KostDiscussionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = KostDiscussion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create(),
            'kost_id' => Kost::factory()->create(),
            'text' => $this->faker->text(),
        ];
    }

    /**
     * Indicate that the model's reply.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function reply()
    {
        return $this->state(function (array $attributes) {
            return [
                'reply_id' => KostDiscussion::factory()->create(),
            ];
        });
    }
}
