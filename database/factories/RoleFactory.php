<?php

namespace Database\Factories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->name();

        return [
            'name' => $name,
            'slug' => Str::slug($name),
            'type' => Role::TYPE_OWNER,
        ];
    }

    /**
     * Indicate that the model's regular role.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function regular()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => Role::TYPE_REGULAR_USER,
            ];
        });
    }

    /**
     * Indicate that the model's premium role.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function premium()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => Role::TYPE_PREMIUM_USER,
            ];
        });
    }
}
