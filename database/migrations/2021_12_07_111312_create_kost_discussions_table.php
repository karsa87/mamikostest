<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKostDiscussionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kost_discussions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('kost_id')->unsigned();
            $table->bigInteger('reply_id')->unsigned()->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->text('text')->nullable();
            $table->timestamps();

            $table->foreign('reply_id')->references('id')->on('kost_discussions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kost_discussions');
    }
}
