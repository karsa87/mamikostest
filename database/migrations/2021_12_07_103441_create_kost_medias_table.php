<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKostMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kost_medias', function (Blueprint $table) {
            $table->bigInteger('kost_id')->unsigned();
            $table->bigInteger('media_id')->unsigned();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('kost_id')->references('id')->on('kosts')->onDelete('cascade');
            $table->foreign('media_id')->references('id')->on('medias')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            $table->primary(['kost_id', 'media_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kost_medias');
    }
}
