<?php

namespace Tests\Feature\Api\KostDiscussion;

use App\Models\KostDiscussion;
use App\Models\Role;
use App\Models\User;
use App\Policies\RolePolicies;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AddKostDiscussionTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // Role
        $this->seed();
        $this->role_owner = Role::where('type', Role::TYPE_OWNER)->first();
        $this->user_owner = User::factory()->for($this->role_owner)->create();

        $this->role_regular = Role::where('type', Role::TYPE_REGULAR_USER)->first();
        $this->user_regular = User::factory()->for($this->role_regular)->create();

        RolePolicies::define();
    }

    /** @test */
    public function it_can_create_new_resource_must_authorized_user()
    {
        $discuss = KostDiscussion::factory()->make();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->postJson(route('api.kost.discussion.add'), [
            'kost_id' => $discuss->kost_id,
            'text' => $discuss->text,
        ]);

        $response->assertStatus(401);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'Unauthorized',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_sucessfully_add_discussion()
    {
        Sanctum::actingAs($this->user_regular);
        $discuss = KostDiscussion::factory()->make();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->postJson(route('api.kost.discussion.add'), [
            'kost_id' => $discuss->kost_id,
            'text' => $discuss->text,
        ]);

        $response->assertOk();
    }

    /** @test */
    public function it_sucessfully_minus_point_user_after_add_discussion_another_owner_user()
    {
        $point_old = $this->user_regular->point;
        Sanctum::actingAs($this->user_regular);
        $discuss = KostDiscussion::factory()->make();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->postJson(route('api.kost.discussion.add'), [
            'kost_id' => $discuss->kost_id,
            'text' => $discuss->text,
        ]);

        $response->assertOk();

        $this->user_regular->refresh();
        $this->assertEquals(($point_old - config('default.point.discussion.credit')), $this->user_regular->point);
    }

    /** @test */
    public function it_can_not_add_discussion_if_have_not_point()
    {
        $user = User::factory()->for($this->role_regular)->create([
            'point' => 0,
        ]);

        Sanctum::actingAs($user);
        $discuss = KostDiscussion::factory()->make();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->postJson(route('api.kost.discussion.add'), [
            'kost_id' => $discuss->kost_id,
            'text' => $discuss->text,
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'messages' => [
                'You dont have enough point for ask',
            ],
        ]);
    }

    /** @test */
    public function it_can_add_discus_even_though_you_dont_have_points_if_that_owner()
    {
        $user = User::factory()->for($this->role_owner)->create([
            'point' => 0,
        ]);

        Sanctum::actingAs($user);
        $discuss = KostDiscussion::factory()->make();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->postJson(route('api.kost.discussion.add'), [
            'kost_id' => $discuss->kost_id,
            'text' => $discuss->text,
        ]);

        $response->assertOk();
    }
}
