<?php

namespace Tests\Feature\Api\Auth;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // Role
        $this->role_owner = Role::factory()->create();
        $this->role_regular = Role::factory()->regular()->create();
        $this->role_premium = Role::factory()->premium()->create();
    }

    /** @test */
    public function it_can_not_register_if_api_key_doesnt_match()
    {
        $user = User::factory()->for($this->role_owner)->create();

        $response = $this->withHeaders([
            'x-api-key' => '2414241',
            'Accept' => 'application/json',
        ])->postJson('api/login', [
            'email' => $user->email,
            'password' => $user->password,
        ]);

        $response->assertStatus(401)
        ->assertExactJson([
            'status' => false,
            'messages' => [
                'Unauthorized',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_can_not_login_if_required_field_empty()
    {
        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/login', []);

        $response->assertStatus(422)
        ->assertExactJson([
            'status' => false,
            'messages' => [
                'The email field is required.',
                'The password field is required.',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_email_must_valid_format()
    {
        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/login', [
            'email' => 'karsaemail',
            'password' => 'P4ssword',
        ]);

        $response->assertStatus(422)
        ->assertExactJson([
            'status' => false,
            'messages' => [
                'The email must be a valid email address.',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_can_not_login_email_and_password_does_not_match()
    {
        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/login', [
            'email' => 'karsa@gmail.com',
            'password' => 'karsa',
        ]);

        $response->assertStatus(401)
        ->assertExactJson([
            'status' => false,
            'messages' => [
                'Invalid login details',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_can_not_login_if_user_status_deactivated()
    {
        $user = User::factory()->deactive()->for($this->role_owner)->create();
        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/login', [
            'email' => $user->email,
            'password' => 'P4ssword',
        ]);

        $response->assertStatus(401)
        ->assertExactJson([
            'status' => false,
            'messages' => [
                'User account deactivated',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_successfully_login_owner_user()
    {
        $user = User::factory()->for($this->role_owner)->create();
        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/login', [
            'email' => $user->email,
            'password' => 'P4ssword',
        ]);

        $response->assertStatus(200);
        $result = $response->json();
        $this->assertTrue($result['status']);
    }

    /** @test */
    public function it_successfully_login_regular_user()
    {
        $user = User::factory()->for($this->role_regular)->create();
        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/login', [
            'email' => $user->email,
            'password' => 'P4ssword',
        ]);

        $response->assertStatus(200);
        $result = $response->json();
        $this->assertTrue($result['status']);
    }

    /** @test */
    public function it_successfully_login_premium_user()
    {
        $user = User::factory()->for($this->role_premium)->create();
        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/login', [
            'email' => $user->email,
            'password' => 'P4ssword',
        ]);

        $response->assertStatus(200);
        $result = $response->json();
        $this->assertTrue($result['status']);
    }
}
