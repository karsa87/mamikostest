<?php

namespace Tests\Feature\Api\Auth;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // Role
        $this->role_owner = Role::factory()->create();
        $this->role_regular = Role::factory()->regular()->create();
        $this->role_premium = Role::factory()->premium()->create();
    }

    /** @test */
    public function it_can_not_register_if_api_key_doesnt_match()
    {
        $user = User::factory()->for($this->role_owner)->make();

        $response = $this->withHeaders([
            'x-api-key' => '2414241',
            'Accept' => 'application/json',
        ])->postJson('api/register', [
            'name' => $user->name,
            'password' => $user->password,
            'email' => $user->email,
            'phone' => $user->phone,
            'address' => $user->address,
            'role' => Role::TYPE_OWNER,
        ]);

        $response->assertStatus(401);
    }

    /** @test */
    public function it_can_not_register_if_required_field_empty()
    {
        $user = User::factory()->for($this->role_owner)->make();

        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/register', [
            'phone' => $user->phone,
            'address' => $user->address,
        ]);

        $response->assertStatus(422)
        ->assertExactJson([
            'status' => false,
            'messages' => [
                'The name field is required.',
                'The password field is required.',
                'The email field is required.',
                'The role field is required.',
            ],
            'data' => [],
        ]);

        $result = $response->json();
        $this->assertFalse($result['status']);
    }

    /** @test */
    public function it_password_must_valid_format()
    {
        $user = User::factory()->for($this->role_owner)->make();

        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/register', [
            'name' => $user->name,
            'password' => 'password',
            'email' => $user->email,
            'phone' => $user->phone,
            'address' => $user->address,
            'role' => Role::TYPE_OWNER,
        ]);

        $response->assertStatus(422)
        ->assertExactJson([
            'status' => false,
            'messages' => [
                'Invalid password, minimum 7 characters containing uppercase, lowercase and numbers',
            ],
            'data' => [],
        ]);

        $result = $response->json();
        $this->assertFalse($result['status']);
    }

    /** @test */
    public function it_email_must_valid_format()
    {
        $user = User::factory()->for($this->role_owner)->make();

        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/register', [
            'name' => $user->name,
            'password' => $user->password,
            'email' => 'emailkarsa',
            'phone' => $user->phone,
            'address' => $user->address,
            'role' => Role::TYPE_OWNER,
        ]);

        $response->assertStatus(422)
        ->assertExactJson([
            'status' => false,
            'messages' => [
                'The email must be a valid email address.',
            ],
            'data' => [],
        ]);

        $result = $response->json();
        $this->assertFalse($result['status']);
    }

    /** @test */
    public function it_role_only_allow_owner_regular_premium_role()
    {
        $user = User::factory()->for($this->role_owner)->make();

        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/register', [
            'name' => $user->name,
            'password' => $user->password,
            'email' => $user->email,
            'phone' => $user->phone,
            'address' => $user->address,
            'role' => 9999999,
        ]);

        $response->assertStatus(422)
        ->assertExactJson([
            'status' => false,
            'messages' => [
                'The selected role is invalid.',
            ],
            'data' => [],
        ]);

        $result = $response->json();
        $this->assertFalse($result['status']);
    }

    /** @test */
    public function it_successfully_register_owner_user()
    {
        $user = User::factory()->for($this->role_owner)->make();

        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/register', [
            'name' => $user->name,
            'password' => 'P4ssword',
            'email' => $user->email,
            'phone' => $user->phone,
            'address' => $user->address,
            'role' => $user->role->type,
        ]);

        $response->assertStatus(200);
        $result = $response->json();
        $this->assertTrue($result['status']);

        $this->assertDatabaseHas('users', [
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'address' => $user->address,
            'role_id' => $this->role_owner->id,
        ]);

        $userRegister = User::where('email', $user->email)->first();
        $this->assertTrue($userRegister->is_owner);
    }

    /** @test */
    public function it_successfully_register_regular_user()
    {
        $user = User::factory()->for($this->role_regular)->make();

        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/register', [
            'name' => $user->name,
            'password' => 'P4ssword',
            'email' => $user->email,
            'phone' => $user->phone,
            'address' => $user->address,
            'role' => $user->role->type,
        ]);

        $response->assertStatus(200);
        $result = $response->json();
        $this->assertTrue($result['status']);

        $this->assertDatabaseHas('users', [
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'address' => $user->address,
            'role_id' => $this->role_regular->id,
            'point' => config('default.point.user.regular'),
        ]);

        $userRegister = User::where('email', $user->email)->first();
        $this->assertTrue($userRegister->is_regular);
    }

    /** @test */
    public function it_successfully_register_premium_user()
    {
        $user = User::factory()->for($this->role_premium)->make();

        $response = $this->withHeaders([
            'x-api-key' => config('sanctum.secret_key'),
            'Accept' => 'application/json',
        ])->postJson('api/register', [
            'name' => $user->name,
            'password' => 'P4ssword',
            'email' => $user->email,
            'phone' => $user->phone,
            'address' => $user->address,
            'role' => $user->role->type,
        ]);

        $response->assertStatus(200);
        $result = $response->json();
        $this->assertTrue($result['status']);

        $this->assertDatabaseHas('users', [
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'address' => $user->address,
            'role_id' => $this->role_premium->id,
            'point' => config('default.point.user.premium'),
        ]);

        $userRegister = User::where('email', $user->email)->first();
        $this->assertTrue($userRegister->is_premium);
    }
}
