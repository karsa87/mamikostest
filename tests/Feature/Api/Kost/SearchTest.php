<?php

namespace Tests\Feature\Api\Kost;

use App\Models\Kost;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Arr;
use Tests\TestCase;

class SearchTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // Role
        $this->role_owner = Role::factory()->create();
    }

    /** @test */
    public function it_not_authentication_user_can_access()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson('api/kost/search');

        $response->assertOk();
    }

    /** @test */
    public function it_display_listing_kost()
    {
        $user = User::factory()->for($this->role_owner)->create();
        $kosts = Kost::factory(5)->for($user, 'owner')->create();

        $user2 = User::factory()->for($this->role_owner)->create();
        $kosts2 = Kost::factory(5)->for($user2, 'owner')->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson('api/kost/search');

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        foreach ($kosts as $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNotNull($exists);
        }

        foreach ($kosts2 as $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNotNull($exists);
        }
    }

    /** @test */
    public function it_display_listing_kost_with_filter_name()
    {
        $user = User::factory()->for($this->role_owner)->create();
        $kosts = Kost::factory(5)->for($user, 'owner')->create();

        $kost = $kosts->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.search', [
            'name' => $kost->name,
        ]));

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        $exists = Arr::first($lists, function ($v, $k) use ($kost) {
            return $kost->slug == $v['slug'];
        });

        $this->assertNotNull($exists);

        foreach ($kosts->where('id', '!=', $kost->id) as $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNull($exists);
        }
    }

    /** @test */
    public function it_display_listing_kost_with_filter_city()
    {
        $user = User::factory()->for($this->role_owner)->create();
        $kosts = Kost::factory(5)->for($user, 'owner')->create();

        $kost = $kosts->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.search', [
            'city' => $kost->city,
        ]));

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        $exists = Arr::first($lists, function ($v, $k) use ($kost) {
            return $kost->slug == $v['slug'];
        });

        $this->assertNotNull($exists);

        foreach ($kosts->where('id', '!=', $kost->id) as $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNull($exists);
        }
    }

    /** @test */
    public function it_display_listing_kost_with_filter_province()
    {
        $user = User::factory()->for($this->role_owner)->create();
        $kosts = Kost::factory(5)->for($user, 'owner')->create();

        $kost = $kosts->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.search', [
            'province' => $kost->province,
        ]));

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        $exists = Arr::first($lists, function ($v, $k) use ($kost) {
            return $kost->slug == $v['slug'];
        });

        $this->assertNotNull($exists);

        foreach ($kosts->where('id', '!=', $kost->id) as $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNull($exists);
        }
    }

    /** @test */
    public function it_display_listing_kost_with_filter_country()
    {
        $user = User::factory()->for($this->role_owner)->create();
        $kosts = Kost::factory(5)->for($user, 'owner')->create();

        $kost = $kosts->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.search', [
            'country' => $kost->country,
        ]));

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        $exists = Arr::first($lists, function ($v, $k) use ($kost) {
            return $kost->slug == $v['slug'];
        });

        $this->assertNotNull($exists);

        foreach ($kosts->where('id', '!=', $kost->id) as $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNull($exists);
        }
    }

    /** @test */
    public function it_display_listing_kost_with_filter_range_price()
    {
        $user = User::factory()->for($this->role_owner)->create();
        $kosts = Kost::factory(5)->for($user, 'owner')->create();
        $kosts = $kosts->sortBy('price');

        $kost = $kosts->first();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.search', [
            'min-price' => $kost->price - 1000,
            'max-price' => $kost->price,
        ]));

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        $exists = Arr::first($lists, function ($v, $k) use ($kost) {
            return $kost->slug == $v['slug'];
        });

        $this->assertNotNull($exists);

        foreach ($kosts->where('id', '!=', $kost->id) as $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNull($exists);
        }
    }

    /** @test */
    public function it_display_listing_kost_with_sorting_ascending_by_price()
    {
        $user = User::factory()->for($this->role_owner)->create();
        $kosts = Kost::factory(5)->for($user, 'owner')->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.search', [
            'sort' => 'asc',
            'sortBy' => 'price',
        ]));

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        foreach ($kosts->sortBy('price')->values() as $k => $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNotNull($exists);

            $this->assertEquals($kost->slug, $lists[$k]['slug']);
        }
    }

    /** @test */
    public function it_display_listing_kost_with_sorting_descending_by_price()
    {
        $user = User::factory()->for($this->role_owner)->create();
        $kosts = Kost::factory(5)->for($user, 'owner')->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.search', [
            'sort' => 'desc',
            'sortBy' => 'price',
        ]));

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        foreach ($kosts->sortByDesc('price')->values() as $k => $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNotNull($exists);

            $this->assertEquals($kost->slug, $lists[$k]['slug']);
        }
    }

    /** @test */
    public function it_display_listing_kost_with_pagination()
    {
        $kosts = collect();
        $user = User::factory()->for($this->role_owner)->create();
        $kosts_user = Kost::factory(5)->for($user, 'owner')->create();
        $kosts = $kosts->merge($kosts_user);

        $user2 = User::factory()->for($this->role_owner)->create();
        $kosts_user = Kost::factory(5)->for($user2, 'owner')->create();
        $kosts = $kosts->merge($kosts_user);

        $user3 = User::factory()->for($this->role_owner)->create();
        $kosts_user = Kost::factory(5)->for($user3, 'owner')->create();
        $kosts = $kosts->merge($kosts_user);

        $user4 = User::factory()->for($this->role_owner)->create();
        $kosts_user = Kost::factory(5)->for($user4, 'owner')->create();
        $kosts = $kosts->merge($kosts_user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.search', [
            'page' => 2,
        ]));

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        foreach ($kosts->sortBy('price')->skip(15) as $k => $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNotNull($exists);
        }
    }
}
