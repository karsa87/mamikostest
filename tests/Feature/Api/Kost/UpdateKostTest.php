<?php

namespace Tests\Feature\Api\Kost;

use App\Models\Kost;
use App\Models\Media;
use App\Models\Role;
use App\Models\User;
use App\Policies\RolePolicies;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UpdateKostTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // Role
        $this->seed();
        $this->role_owner = Role::where('type', Role::TYPE_OWNER)->first();
        $this->role_regular = Role::where('type', Role::TYPE_REGULAR_USER)->first();
        $this->user = User::factory()->for($this->role_owner)->create();
        $this->kost = Kost::factory()->for($this->user, 'owner')->create();

        RolePolicies::define();
    }

    /** @test */
    public function it_can_update_resource_must_authorized_user()
    {
        $kost = $this->kost;

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->putJson(route('api.kost.update', ['kost' => $this->kost->id]), [
            'name' => $kost->name,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => $kost->price,
            'description' => $kost->description,
            'medias' => [],
        ]);

        $response->assertStatus(401);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'Unauthorized',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_can_update_resource_must_have_access_user()
    {
        $user = User::factory()->for($this->role_regular)->create();
        Sanctum::actingAs($user);
        $kost = $this->kost;

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->putJson(route('api.kost.update', ['kost' => $this->kost->id]), [
            'name' => $kost->name,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => $kost->price,
            'description' => $kost->description,
            'medias' => [],
        ]);

        $response->assertStatus(401);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'You dont have access',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_can_not_update_resource_another_owner()
    {
        $user = User::factory()->for($this->role_owner)->create();
        Sanctum::actingAs($user);
        $kost = $this->kost;

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->putJson(route('api.kost.update', ['kost' => $this->kost->id]), [
            'name' => $kost->name,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => $kost->price,
            'description' => $kost->description,
            'medias' => [],
        ]);

        $response->assertStatus(401);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'You dont have access update this kost detail',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_can_not_update_resource_with_empty_required_field()
    {
        Sanctum::actingAs($this->user);
        $kost = Kost::factory()->make();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->putJson(route('api.kost.update', ['kost' => $this->kost->id]), [
            'country' => $kost->country,
            'description' => $kost->description,
            'medias' => [],
        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'The name field is required.',
                'The slug field is required.',
                'The address field is required.',
                'The city field is required.',
                'The province field is required.',
                'The price field is required.',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_name_must_unique()
    {
        Sanctum::actingAs($this->user);
        $kost_exists = Kost::factory()->create();
        $kost = Kost::factory()->make();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->putJson(route('api.kost.update', ['kost' => $this->kost->id]), [
            'name' => $kost_exists->name,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => $kost->price,
            'description' => $kost->description,
            'medias' => [],
        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'The slug has already been taken.',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_price_must_numeric()
    {
        Sanctum::actingAs($this->user);
        $kost = Kost::factory()->make();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->putJson(route('api.kost.update', ['kost' => $this->kost->id]), [
            'name' => $kost->name,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => 'dua puluh juta',
            'description' => $kost->description,
            'medias' => [],
        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'The price must be a number.',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_price_must_numeric_with_min_zero()
    {
        Sanctum::actingAs($this->user);
        $kost = Kost::factory()->make();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->putJson(route('api.kost.update', ['kost' => $this->kost->id]), [
            'name' => $kost->name,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => -20000,
            'description' => $kost->description,
            'medias' => [],
        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'The price must be at least 0.',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_medias_must_array_value()
    {
        Sanctum::actingAs($this->user);
        $kost = Kost::factory()->make();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->putJson(route('api.kost.update', ['kost' => $this->kost->id]), [
            'name' => $kost->name,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => $kost->price,
            'description' => $kost->description,
            'medias' => 1,
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'messages' => [
                'The medias must array with numeric value.',
            ],
        ]);
    }

    /** @test */
    public function it_medias_must_array_with_value_numeric()
    {
        Sanctum::actingAs($this->user);
        $kost = Kost::factory()->make();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->putJson(route('api.kost.update', ['kost' => $this->kost->id]), [
            'name' => $kost->name,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => $kost->price,
            'description' => $kost->description,
            'medias' => [
                'id:1',
            ],
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'messages' => [
                'The medias.0 must be a number.',
            ],
        ]);
    }

    /** @test */
    public function it_successfully_update_resource()
    {
        Sanctum::actingAs($this->user);
        $kost = Kost::factory()->make();
        $medias = Media::factory(5)->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->putJson(route('api.kost.update', ['kost' => $this->kost->id]), [
            'name' => $kost->name,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => $kost->price,
            'description' => $kost->description,
            'medias' => $medias->pluck('id'),
        ]);

        $response->assertOk();
        $this->assertDatabaseHas('kosts', [
            'name' => $kost->name,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => $kost->price,
            'description' => $kost->description,
        ]);

        $kost = Kost::where([
            'name' => $kost->name,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => $kost->price,
            'description' => $kost->description,
        ])->first();

        foreach ($medias as $media) {
            $this->assertDatabaseHas('kost_medias', [
                'kost_id' => $kost->id,
                'media_id' => $media->id,
            ]);
        }
    }
}
