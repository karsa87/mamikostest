<?php

namespace Tests\Feature\Api\Kost;

use App\Models\Kost;
use App\Models\Role;
use App\Models\User;
use App\Policies\RolePolicies;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class DeleteKostTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // Role
        $this->seed();
        $this->role_owner = Role::where('type', Role::TYPE_OWNER)->first();
        $this->role_regular = Role::where('type', Role::TYPE_REGULAR_USER)->first();
        $this->user = User::factory()->for($this->role_owner)->create();
        $this->kost = Kost::factory()->for($this->user, 'owner')->create();

        RolePolicies::define();
    }

    /** @test */
    public function it_can_delete_resource_must_authorized_user()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->deleteJson(route('api.kost.delete', ['kost' => $this->kost->id]));

        $response->assertStatus(401);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'Unauthorized',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_can_delete_resource_must_have_access_user()
    {
        $user = User::factory()->for($this->role_regular)->create();
        Sanctum::actingAs($user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->deleteJson(route('api.kost.delete', ['kost' => $this->kost->id]));

        $response->assertStatus(401);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'You dont have access delete this kost',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_can_not_delete_resource_another_owner()
    {
        $user = User::factory()->for($this->role_owner)->create();
        Sanctum::actingAs($user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->deleteJson(route('api.kost.delete', ['kost' => $this->kost->id]));

        $response->assertStatus(401);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'You dont have access delete this kost',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_successfully_delete_resource()
    {
        Sanctum::actingAs($this->user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->deleteJson(route('api.kost.delete', ['kost' => $this->kost->id]));

        $response->assertOk();
        $this->assertDatabaseMissing('kosts', [
            'name' => $this->kost->name,
            'phone' => $this->kost->phone,
            'address' => $this->kost->address,
            'city' => $this->kost->city,
            'province' => $this->kost->province,
            'country' => $this->kost->country,
            'price' => $this->kost->price,
            'description' => $this->kost->description,
        ]);
    }
}
