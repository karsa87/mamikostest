<?php

namespace Tests\Feature\Api\Kost;

use App\Models\Kost;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Arr;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ListTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // Role
        $this->role_owner = Role::factory()->create();
        $this->role_regular = Role::factory()->regular()->create();
        $this->role_premium = Role::factory()->premium()->create();
    }

    /** @test */
    public function it_must_authentication_user()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson('api/kost/list');

        $response->assertStatus(401);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'Unauthorized',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_display_listing_kost()
    {
        $user = User::factory()->for($this->role_owner)->create();
        Kost::factory(5)->for($user, 'owner')->create();
        Sanctum::actingAs($user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson('api/kost/list');

        $response->assertOk();
    }

    /** @test */
    public function it_only_display_listing_kost_of_his_owner()
    {
        $user = User::factory()->for($this->role_owner)->create();
        $user_owner2 = User::factory()->for($this->role_owner)->create();
        $kosts = Kost::factory(5)->for($user, 'owner')->create();
        $kosts_owner2 = Kost::factory(5)->for($user_owner2, 'owner')->create();
        Sanctum::actingAs($user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson('api/kost/list');

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        foreach ($kosts as $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNotNull($exists);
        }

        foreach ($kosts_owner2 as $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNull($exists);
        }
    }

    /** @test */
    public function it_empty_display_listing_kost_of_regular_user()
    {
        $user = User::factory()->for($this->role_owner)->create();
        $kosts = Kost::factory(5)->for($user, 'owner')->create();

        $user_regular = User::factory()->for($this->role_regular)->create();
        Sanctum::actingAs($user_regular);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson('api/kost/list');

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        $this->assertCount(0, $lists);
    }

    /** @test */
    public function it_display_listing_kost_with_pagination()
    {
        $kosts = collect();
        $user = User::factory()->for($this->role_owner)->create();
        $kosts = Kost::factory(20)->for($user, 'owner')->create();
        Sanctum::actingAs($user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.list', [
            'page' => 2,
        ]));

        $response->assertOk();
        $result = $response->json();
        $lists = $result[0]['data']['list'];

        foreach ($kosts->sortBy('price')->skip(15) as $k => $kost) {
            $exists = Arr::first($lists, function ($v, $k) use ($kost) {
                return $kost->slug == $v['slug'];
            });

            $this->assertNotNull($exists);
        }
    }
}
