<?php

namespace Tests\Feature\Api\Kost;

use App\Models\Media;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class MediaTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // Role
        $this->role_owner = Role::factory()->create();
        $this->user = User::factory()->for($this->role_owner)->create();
    }

    /** @test */
    public function it_must_authentication_user()
    {
        $media = Media::factory()->make();
        $file = UploadedFile::fake()->image('kamar-kost1.jpg');

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->postJson(route('api.kost.upload.media'), [
            'title' => $media->title,
            'description' => $media->description,
            'media' => $file,
        ]);

        $response->assertStatus(401);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'Unauthorized',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_successfully_upload_media()
    {
        $media = Media::factory()->make();
        $file = UploadedFile::fake()->image('kamar-kost1.jpg');
        Sanctum::actingAs($this->user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->postJson(route('api.kost.upload.media'), [
            'title' => $media->title,
            'description' => $media->description,
            'media' => $file,
        ]);

        $response->assertOk();
        $this->assertDatabaseHas('medias', [
            'title' => $media->title,
            'description' => $media->description,
        ]);
    }

    /** @test */
    public function it_failed_upload_media_with_not_supported_file_extenstion()
    {
        $media = Media::factory()->make();
        $file = UploadedFile::fake()->image('kamar-kost1.xls');
        Sanctum::actingAs($this->user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->postJson(route('api.kost.upload.media'), [
            'title' => $media->title,
            'description' => $media->description,
            'media' => $file,
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'messages' => [
                'Invalid format file',
            ],
        ]);
    }
}
