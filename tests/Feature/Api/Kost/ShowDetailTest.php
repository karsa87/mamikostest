<?php

namespace Tests\Feature\Api\Kost;

use App\Models\Kost;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ShowDetailTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // Role
        $this->role_owner = Role::factory()->create();
        $this->user = User::factory()->for($this->role_owner)->create();
        $this->kost = Kost::factory()->for($this->user, 'owner')->create();
    }

    /** @test */
    public function it_not_authentication_user_can_access()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.show', [
            'slug' => $this->kost->slug,
        ]));

        $response->assertOk();
    }

    /** @test */
    public function it_not_display_detail_kost_if_not_found()
    {
        $kost = Kost::factory()->for($this->user, 'owner')->make();
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.show', [
            'slug' => $kost->slug,
        ]));

        $response->assertStatus(404);
        $response->assertExactJson([
            'status' => false,
            'messages' => [
                'The resource not found',
            ],
            'data' => [],
        ]);
    }

    /** @test */
    public function it_display_detail_kost()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->getJson(route('api.kost.show', [
            'slug' => $this->kost->slug,
        ]));

        $response->assertOk();

        $kost = $this->kost;
        $data = [
            'name' => $kost->name,
            'slug' => $kost->slug,
            'phone' => $kost->phone,
            'address' => $kost->address,
            'city' => $kost->city,
            'province' => $kost->province,
            'country' => $kost->country,
            'price' => round($kost->price, 2),
            'description' => $kost->description,
            'owner' => [
                'name' => $kost->owner->name,
                'email' => $kost->owner->email,
                'phone' => $kost->owner->phone,
                'address' => $kost->owner->address,
            ],
            'medias' => [],
            'kost_discussions' => [],
        ];

        foreach ($kost->medias as $media) {
            $data['medias'][] = [
                'title' => $media->title,
                'description' => $media->description,
                'path' => $media->path,
                'full_url' => $media->full_url,
            ];
        }

        foreach ($kost->kostDiscussions as $discuss) {
            $v = [
                'user' => [
                    'name' => $discuss->user->name,
                    'email' => $discuss->user->email,
                    'phone' => $discuss->user->phone,
                ],
                'text' => $discuss->text,
            ];

            if ($discuss->reply) {
                $v['reply'] = [
                    'user' => [
                        'name' => $discuss->reply->user->name,
                        'email' => $discuss->reply->user->email,
                        'phone' => $discuss->reply->user->phone,
                    ],
                    'text' => $discuss->reply->text,
                ];
            }

            $data['kost_discussions'][] = $v;
        }
        $response->assertExactJson([
            'status' => true,
            'messages' => [],
            'data' => $data,
        ]);
    }
}
