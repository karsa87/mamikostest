<?php

namespace Tests\Feature\Command;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ResetUserPointTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_cancel_command_reset_user_point()
    {
        $this->artisan('reset:user-point')
                ->expectsConfirmation('Are you sure for reset point all user ?', 'no')
                ->assertExitCode(Command::FAILURE);
    }

    /** @test */
    public function it_successfully_reset_user_point()
    {
        $role_owner = Role::factory()->create();
        $role_regular = Role::factory()->regular()->create();
        $role_premium = Role::factory()->premium()->create();

        $owner = User::factory()->for($role_owner)->create();
        $regular = User::factory()->for($role_regular)->create();
        $premium = User::factory()->for($role_premium)->create();

        $this->artisan('reset:user-point')
                ->expectsConfirmation('Are you sure for reset point all user ?', 'yes')
                ->assertExitCode(Command::SUCCESS);

        $this->assertDatabaseHas('users', [
            'id' => $owner->id,
            'point' => 0,
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $regular->id,
            'point' => config('default.point.user.regular'),
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $premium->id,
            'point' => config('default.point.user.premium'),
        ]);
    }
}
